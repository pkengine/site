@extends('admin::layout')

@section('main-content')
    <div id="admin">
        <layout></layout>
    </div>
    <script src="{{ Front::assetV('/js/admin.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ Front::assetV('/css/admin.css')}}">
@endsection
