import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        user: null,
        env: null,
        menu: [],
        categories: {},
        routeItem: null,
        url: null,
        pageTemplates: [],
        hasTemplates: {},
        langs: [],
        app: {}
    },
    mutations: {
        set: (state, value) => {
            _.each(_.keys(value), (key) => {
                state[key] = value[key]
            })
        },
        hasTemplates: (state, value) => {
            if(!state.hasTemplates[value[0]]){
                state.hasTemplates[value[0]] = value[1]
            }
        }
    },
    getters: {
        app: (state) => { return state.app},
        user: (state) => { return state.user},
        menu: (state) => { return state.menu},
        env: (state) => {return state.env },
        routeItem: (state) => {return state.routeItem },
        categories: (state) => {return state.categories },
        url: (state) => {return state.url },
        pageTemplates: (state) => {return state.pageTemplates },
        hasTemplates: (state) => {return state.hasTemplates },
        langs: (state) => {return state.langs }
    },
    actions: {

    }
})

export default store
