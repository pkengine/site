export default (editor, url) => {

    // Add a button that opens a window
    editor.ui.registry.addButton('link', {
        icon: 'link',
        onAction:  () => {
            // Open window

            window.$app.$emit('tinymce:link:open')
        }
    });
    return {
        getMetadata: function () {
            return  {
                name: "Link plugin",
            };
        }
    };
}
