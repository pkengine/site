export default (editor, url) => {

    // Add a button that opens a window
    editor.ui.registry.addButton('media', {
        icon: 'image',
        onAction: function () {
            // Open window
            window.$app.$emit('tinymce:media:open')
        }
    });
    // editor.ui.registry.addContextMenu('example', {
    //     text: 'Example plugin',
    //     onAction: function() {
    //         // Open window
    //         console.log(this)
    //     }
    // });
    return {
        getMetadata: function () {
            return  {
                name: "Media plugin",
            };
        }
    };
}
