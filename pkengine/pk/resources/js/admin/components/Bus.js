import Vue from 'vue';

export default {
    install(Vue) {
        Vue.bus = Vue.prototype.$bus = new Vue();
    }
}
