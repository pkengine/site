import DeleteConfirm from "../components/DeleteConfirm";

export var validator = {
    data() {
        return {
            errors: {},
            responseStatusCode: null,
        }
    },
    methods: {
        validatorErrors(error, closure) {
            if (_.has(error, 'response.data.errors')) {
                if(closure){
                    closure(_.mapValues(_.get(error, 'response.data.errors', {}), 0))
                }else{
                    this.errors = _.mapValues(_.get(error, 'response.data.errors', {}), 0);
                }
            }
            if(this.$notify){
                if(_.has(error, 'response.data.text')){
                    this.$notify({message: _.get(error, 'response.data.text'), color: 'error'});
                }else{
                    let res = this.parseError(error);
                    this.$notify({message: this.$t('error') + ': ' + res, color: 'error'});
                }
            }
            this.responseStatusCode = this.getStatusCode(error);
        },
        getStatusCode(error){
            return _.get(error, 'response.status', 200);
        },
        parseError(error) {
            let res = this.getStatusCode(error);
            if (res === 421) return 'Misdirected Request';
            if (res === 422) return 'Некорректный формат записи';
            if (res === 423) return 'Locked';
            if (res === 424) return 'Failed Dependency';
            if (res === 429) return 'Слишком много неудачных попыток';
            if (res === 403) return 'Нет доступа';
            if (res === 500) return 'Ошибка на сервере';
            return res;
        },
    }
};

export var crudDefault = {
    mixins: [validator],
    data() {
        return {
            model: null,
            route: '#',
            dataItems: [],
            addEditId: false,
            editableModel: {},
            hasEditModel: false,
            hasCreateModel: false,
            hasRightSide: false,
            afterEditModel: false,
            afterCreateModel: false,
            filterStorage: {},
            defaultModel: {},
        }
    },
    methods: {
        addItemList(item) {
            if (this.dataItems) {
                this.dataItems.unshift(item);
            }
            if(this.pagination){
                this.pagination.itemsPerPage++
            }
        },
        updateItemList(item) {
            if (this.dataItems) {
                this.dataItems = _.map(this.dataItems, (o) => {
                    if (o.id == item.id) {
                        return item;
                    }
                    return o;
                });
            }
        },
        setRouteItem(value) {
            this.$store.commit('set', {routeItem: value})
        },
        editorModelSupport(){
            //consoleLog('default editorModelSupport');
        },
        clearEditorModelSupport(){
            //consoleLog('default clearEditorModelSupport');
        }
    },
    watch:{
        model(value){
            this.setRouteItem(value)
        },
    }
};

export var pagination = {
    mixins: [crudDefault, validator],
    data() {
        return {
            route: '#',
            params: {},
            dataItems: [],
            responseData: {},
            hasRightSide: false,
            propsPagination: {},
            pagination: {
                total: -1,
                itemsPerPageOption: [15, 30, 60],
                page: 1,
                pages: 0,
                itemsPerPage: 15,
                sortBy: ['id'],
                sortDesc: [true],
            },
            isQuery: true,
            s: '',
            queueQuery: null,
            queryTimer: 0,
            lastQuery: ''
        }
    },
    watch: {
        s(value) {
            this.pagination.page = 1
            this.getItems()
        },
        pagination: {
            handler() {
                this.getItems()
            },
            deep: true
        },
        filter: {
            handler(value) {
                this.pagination.page = 1
                this.getItems()
            },
            deep: true
        },
    },
    methods: {
        getItems(data) {
            let params = {};
            let page = data ? data.page : (this.pagination.page ? this.pagination.page : 1);
            let perPage = data ? data.itemsPerPage : this.pagination.itemsPerPage;
            params = _.merge({
                s: this.s,
                page: page,
                desc: this.pagination.sortDesc[0],
                sort_by: this.pagination.sortBy[0],
                per_page: perPage
            }, this.params, this.filter);
            let lastQuery =  JSON.stringify(params);
            if(this.lastQuery == lastQuery){
                return false
            }
            this.lastQuery = lastQuery
            this.isQuery = false
            if(this.queueQuery){
                clearTimeout(this.queueQuery);
                this.queryTimer = 500
            }
            this.queueQuery = setTimeout(() => {
                //Query
                this.lastQuery = JSON.stringify(params)
                axios.get(this.route, {params: params})
                    .then((response) => {
                        if (_.isObject(response.data)) {
                            this.responseData = response.data;
                            this.dataItems = response.data.data
                            let pagination = {
                                page: response.data.current_page,
                                pages: response.data.pages,
                                itemsPerPage: _.toInteger(response.data.per_page),
                                total: response.data.total
                            }
                            this.pagination = _.cloneDeep(_.merge(this.pagination, pagination))
                            if(response.data.current_page > response.data.pages){
                                params.page = response.data.pages
                                this.getItems(null, params)
                                return false;
                            }
                        }
                    })
                    .finally(() => {
                        this.isQuery = true
                        this.queryTimer = 0
                    })
                //end Query
            }, this.queryTimer)

        },
    },
};

export var edit = {
    mixins: [crudDefault, validator],
    data() {
        return {
            editModel: {},
            hasEditModel: false,
            addEditId: false,
            isQueryEdit: true,
            loading: false
        }
    },
    watch: {
        editModel(value) {
            this.setRouteItem(value)
        }
    },
    methods: {
        editItem(id, permission, query) {
            if (!permission || this.$hasPermission(permission)) {
                this.editModel = _.cloneDeep(this.defaultModel);
                this.hasEditModel = false;
                this.hasCreateModel = false;
                this.getEditItem(id, query);
            }
        },
        getEditItem(id, query) {
            this.errors = {};
            if (_.isInteger(id)) {
                if (_.has(this.editModel, 'id') && this.editModel.id == id) {
                    this.hasEditModel = true;
                    this.hasRightSide = true;
                } else {
                    if (!query) {
                        this.isQueryEdit = false
                    }

                    axios.get(this.route + '/' + id + '/edit')
                        .then((response) => {
                            this.editModel = response.data;
                            this.hasEditModel = true;
                            this.hasRightSide = true;
                        })
                        .finally(() => {
                            if (!query) {
                                this.isQueryEdit = true
                            }
                            if (this.addEditId) {
                                let route = '/' + this.addEditId + '/' + id
                                if (route !== this.$route.path) {
                                    this.$router.push(route)
                                        .then(() => {
                                            this.editorModelSupport()
                                        })
                                }else{
                                    this.editorModelSupport()
                                }
                            }
                        })
                }
            } else if (_.isObject(id)) {
                this.editModel = _.clone(id);
                this.hasEditModel = true;
                this.hasRightSide = true;
                if (this.addEditId) {
                    let route = '/' + this.addEditId + '/' + this.editModel.id
                    if (route !== this.$route.path) {
                        this.$router.push(route)
                    }
                }
            }
            this.editableModel = this.editModel;
        },
        setEditItem(model) {
            if(this.loading) return false;
            this.loading = true;
            this.errors = {};
            axios.put(this.route + '/' + this.editModel.id, model || this.editModel)
                .then((response) => {
                    if (response.data.id) {
                        this.$notify({message:  this.$t('saved'), color: 'success'});
                        this.updateItemList(response.data);
                        this.editModel = response.data
                        this.errors = {}
                        this.afterSave(response.data)
                        this.afterEdit(response.data)
                        this.closeEditItem(this.afterEditModel);
                    } else if (response.data.success == 1) {
                        this.$notify({message:  this.$t('saved'), color: 'success'});
                        this.errors = {}
                        this.afterSave(response.data)
                        this.afterEdit(response.data)
                        this.closeEditItem(this.afterEditModel);
                    } else {
                        this.$notify({message: this.$t('error') + ': ' + response.data.text, color: 'error'});
                    }
                })
                .catch((error) => {
                    this.validatorErrors(error);
                })
                .finally(() => {
                    this.loading = false;
                });
        },
        closeEditItem(afterEditModel) {
            afterEditModel = (typeof afterEditModel !== 'undefined') ? (afterEditModel === true) : this.afterEditModel;
            this.hasEditModel = afterEditModel;
            this.hasRightSide = afterEditModel;
            if (!afterEditModel) {
                this.editModel = _.cloneDeep(this.defaultModel);
            }
            if (this.addEditId) {
                let route = '/' + this.addEditId
                if (route !== this.$route.path) {
                    this.$router.push('/' + this.addEditId)
                }
            }
        },
        afterEdit(){},
        afterSave(){},
        isEditItem(id){
           return  this.hasEditModel && this.editModel && this.editModel.id == id;
        },
    },
    created() {

    }
};

export var create = {
    mixins: [crudDefault, validator],
    data() {
        return {
            loading: false,
            createModel: {},
            hasCreateModel: false,
            createHeaders: {},
            defaultModel: {}
        }
    },
    methods: {
        createItem() {
            this.createModel = _.cloneDeep(this.defaultModel)
            this.hasEditModel = false;
            this.hasCreateModel = true;
            this.hasRightSide = true;
            this.errors = {};
            this.editableModel = {};
        },
        setCreateItem() {
            if(this.loading) return false;
            this.loading = true;
            this.errors = {};
            axios.post(this.route, this.createModel, this.createHeaders)
                .then((response) => {
                    if (response.data.id) {
                        this.$notify({message:  this.$t('added'), color: 'success'});
                        this.addItemList(response.data);
                        this.afterSave(response.data);
                        this.afterCreate(response.data);
                        this.closeCreateItem(this.afterCreateModel);
                        this.editModel = response.data;
                        this.hasCreateModel = this.afterCreateModel;
                        this.hasRightSide = this.afterCreateModel;
                    } else if (response.data.success == 1) {
                        this.$notify({message:  this.$t('saved'), color: 'success'});
                        this.afterSave(response.data);
                        this.afterCreate(response.data);
                        this.closeCreateItem(this.afterCreateModel);
                        this.hasCreateModel = this.afterCreateModel;
                        this.hasRightSide = this.afterCreateModel;
                    } else {
                        this.$notify({message: this.$t('error') + ': ' + response.data.text, color: 'error'});
                    }
                })
                .catch((error) => {
                    this.validatorErrors(error);
                })
                .finally(() => {
                    this.loading = false;
                });
        },
        closeCreateItem(afterCreateModel) {
            this.errors = {}
            afterCreateModel = typeof afterCreateModel !== 'undefined' ? (afterCreateModel === true) : this.afterCreateModel;
            this.hasCreateModel = afterCreateModel;
            this.hasRightSide = afterCreateModel;
            if (!afterCreateModel) {
                this.createModel = _.cloneDeep(this.defaultModel);
            }
        },
        afterSave(){},
        afterCreate(){}
    }
};

export var del = {
    mixins: [crudDefault],
    components: {DeleteConfirm},
    data() {
        return {
            loading: false,
            editModel: _.cloneDeep(this.defaultModel),
            hasDeleteModel: false,
            hasHardDeleteModel: false,
            deleteId: null,
            deleteText: '',
            deletePassword: ''
        }
    },
    methods: {
        getDeleteItem(id, text) {
            this.deleteId = id;
            this.deleteText = text;
            this.hasDeleteModel = true;
        },
        getHardDeleteItem(id, text) {
            this.deleteId = id;
            this.deleteText = text;
            this.hasHardDeleteModel = true;
        },
        setDeleteItem(password) {
            let id = this.deleteId || this.editModel.id;
            id = id.id || id
            axios.delete(this.route + '/' + id, {params: {password: password}})
                .then((response) => {
                    if (response.data.success == 1) {
                        this.dataItems = _.filter(this.dataItems, (o) => {
                            return o.id != id;
                        });
                        this.hasEditModel = false;
                        this.hasRightSide = false;
                        this.hasDeleteModel=false;
                        this.hasHardDeleteModel=false;
                        this.$notify({message:  this.$t('deleted'), color: 'success'});
                    } else if (response.data.id) {
                        if (_.has(response.data, 'is_deleted')) {
                            if(this.editModel){
                                this.editModel.is_deleted = true
                            }
                            this.updateItemList(response.data);
                        }else{
                            this.dataItems = _.filter(this.dataItems, (o) => {
                                return o.id != id;
                            });
                        }
                        this.hasEditModel = false;
                        this.hasRightSide = false;
                        this.$notify({message:  this.$t('deleted'), color: 'success'});
                    } else {
                        this.$notify({message: this.$t('error') + ': ' + response.data.text, color: 'error'});
                    }
                })
                .catch((error) => {
                    this.validatorErrors(error);
                })

        },
        setRestoreItem(model) {
            let id = model ? model.id : this.editModel.id
            this.loading = true;
            axios.post(this.route + '/' + id + '/restore')
                .then((response) => {
                    if (response.data.id) {
                        if(this.editModel){
                            this.editModel.is_deleted = false
                        }
                        this.$notify({message: this.$t('restored'), color: 'success'});
                        this.updateItemList(response.data);
                    } else if (response.data.success == 1) {
                        this.$notify({message: this.$t('restored'), color: 'success'});
                    } else {
                        this.$notify({message: this.$t('error') + ': ' + response.data.text, color: 'error'});
                    }
                    this.hasEditModel = false;
                    this.hasRightSide = false;
                })
                .catch((error) => {
                    this.validatorErrors(error);
                })
        }
    }
}

export var validate = {
    data() {
        return {
            errors: {},
        }
    },
    methods: {}
}

export var langs = {
    data(){
      return {
          iLang: null
      }
    },
    watch:{
        'editModel.lang'(value){
            if(typeof this.editModel === 'object'){
                this.iLang = _.findIndex(this.editModel.langs, {'lang': value});
                let lang =_.find(this.editModel.langs, {'lang': value})
                if(lang){
                    this.editModel.title = lang.title
                    this.editModel.description = lang.description
                    this.editModel.content = lang.content
                }
            }
        },
        'editModel.title'(value){
            this.setLangField(value, 'title')
        },
        'editModel.description'(value){
            this.setLangField(value, 'description')
        },
        'editModel.content'(value){
            this.setLangField(value, 'content')
        }
    },
    methods:{
        setLangField(value, field){
            if(this.editModel.langs){
                this.editModel.langs = _.map(_.filter(this.editModel.langs), (lang) => {
                    if(this.editModel.lang === lang.lang && lang[field] !== value){
                        lang[field] = value
                    }
                    return lang
                })
            }

        }
    }
}

export var langSettings = {
    watch:{
        'editModel.lang'(value){
        }
    }
}
