window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.token = document.head.querySelector('meta[name="csrf-token"]');
window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
import _ from 'lodash';
import Vue from 'vue';
import Draggable from 'vuedraggable'
import router from './plugins/router'
import vuetify from './plugins/vuetify'
import store from './plugins/vuex'



require('tinymce')
require('tinymce/themes/silver')
// require('tinymce/plugins/link')
require('tinymce/plugins/image')
require('tinymce/plugins/lists')
require('tinymce/plugins/table')
require('tinymce/plugins/code')
require('tinymce-i18n/langs/ru')
require('tinymce/icons/default/icons')
import Bus from './components/Bus';
import Translate from './components/Translate';
import Permission from './components/Permission';

Vue.use(Bus);
Vue.use(Translate);
Vue.use(Permission);

import DataTable from './components/DataTable'
import TitleHead from './components/TitleHead'

import ModelEditTitle from './components/edit/ModelEditTitle';
import ModelActions from './components/edit/ModelActions';
import ModelEditActions from './components/edit/ModelEditActions';


Vue.component('data-table', DataTable)
Vue.component('title-head', TitleHead)

Vue.component('model-actions', ModelActions)
Vue.component('model-edit-actions', ModelEditActions)
Vue.component('model-edit-title', ModelEditTitle)
Vue.component('draggable', Draggable)
import { mapState } from 'vuex';
import Layout from './Layout'

Vue.mixin({
    data(){
        return {

        }
    },
    methods:{
        hasComponent(name){
            if(_.isArray($app.$customComponents) && $app.$customComponents.indexOf(name) !== -1){
                return true
            }
            return false
        }
    },
    created(){

    }
});

const app  = new Vue({
    el: '#admin',
    vuetify,
    store,
    router,
    components: {Layout},
    computed: mapState(['app', 'user']),
});

Vue.prototype.$notify = function(options){
    this.$bus.$emit('notify', options)
}

Vue.csrf = Vue.prototype.$csrf = token.content
window.$app = app
// import Echo from "laravel-echo"
// window.io = require('socket.io-client');
// window.Echo = new Echo({
//     broadcaster: 'socket.io',
//     host: window.location.hostname + ':6003',
//     auth: {
//         headers: {
//             'X-CSRF-TOKEN': token.content
//         }
//     }
// });
