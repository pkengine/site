<?php

namespace PK\Controllers\Admin;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use PK\Facades\Langs;
use PK\Facades\Settings;
use PK\Helpers\Menu;
use PK\Controllers\Controller;
use PK\Resources\Admin\UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class IndexController extends Controller
{
    public function index()
    {
        return view('admin::index');
    }

    public function data()
    {
        $pageTemplates = collect(scandir(resource_path('/views/app/pages')))
            ->map(function ($item){
                $name = Str::before($item, '.');
                if($name){
                    return [
                        'text' => __('pages.'.$name),
                        'value' => $name
                    ];
                }
                return null;
            })->filter()->values();

        return response()->json([
            'user' => Auth::check() ? new UserResource(Auth::user()) : null,
            'menu' => Menu::get([
                [
                    'title' => 'instance.pages',
                    'url' => '/pages',
                    'icon' => 'newspaper',
                ],
                [
                    'title' => 'instance.posts',
                    'url' => '/posts',
                    'icon' => 'file-alt',
                ],
                [
                    'title' => 'instance.categories',
                    'url' => '/categories',
                    'icon' => 'cabinet-filing',
                ],
                [
                    'title' => 'instance.tags',
                    'url' => '/tags',
                    'icon' => 'tags',
                ],
//                [
//                    'title' => __('admin.routes.taxonomies'),
//                    'url' => '/taxonomies',
//                    'icon' => 'sitemap',
//                ],
                [
                    'title' => 'instance.images',
                    'url' => '/images',
                    'icon' => 'images',
                ],
                [
                    'title' => 'instance.users',
                    'icon' => 'users',
                    'url' => '/users'
                ],
                [
                    'title' => 'instance.settings',
                    'icon' => 'cog',
                    'url' => '/settings'
                ],
                [
                    'title' => 'instance.account',
                    'icon' => 'user',
                    'url' => '/account'
                ],
            ]),
            'pageTemplates' => collect(scandir(resource_path('/views/app/pages')))
                ->map(function ($item){
                    $name = Str::before($item, '.');
                    if($name){
                        return [
                            'text' => __('pages.'.$name),
                            'value' => $name
                        ];
                    }
                    return null;
                })->filter()->values(),
            'pageTemplates' => $pageTemplates,
            'app' => [
                'env' => config('app.env'),
                'title' => config('app.title'),
                'lang' => App::getLocale()
            ],
            'lang' => collect(__('pk::admin'))->merge([

            ])->toArray(),
            'langs' => Langs::get()
        ]);
    }

    public function home()
    {
        return response()->json([]);
    }
}
