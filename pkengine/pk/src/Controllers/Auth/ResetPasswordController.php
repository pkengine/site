<?php


namespace PK\Controllers\Auth;


use Carbon\Carbon;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use PK\Models\User;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    public function recoveryPassword(Request $request)
    {
        $request->validate([
            'email' => 'required|email'
        ]);

        if($user = User::where('email', $request->email)->first()){
            $token = Hash::make(Str::random(64));
            DB::table('password_resets')->insert([
                'token' => $token,
                'email' => $request->email,
                'created_at' => Carbon::now('UTC')
            ]);
            $user->notify(new ResetPassword($token));
        }
        return response()->json(['success' => 1]);
    }

    public function resetPasswordForm(Request $request)
    {
        $request->validate([
            'token' => 'required|string|max:100',
            'email' => 'required|email'
        ]);

        if($token = DB::table('password_resets')
            ->where('token', $request->token)
            ->where('created_at','>', Carbon::now('UTC')->subHours(20))
            ->first()){
            return view('auth.login', ['type' => 'set-password', 'token' => $token->token]);
        }
        return redirect()->route('login.form');

    }

    public function resetPassword(Request $request)
    {
        $request->validate([
            'token' => 'required|string|max:100',
            'password' => 'required|min:6|max:40',
        ]);
        if(($token = DB::table('password_resets')
            ->where('token', $request->token)
            ->where('created_at','>', Carbon::now('UTC')->subHours(20))
            ->first()) &&
            ($user = User::where('email', $token->email))
        ){
            $user->update(['password' => bcrypt($request->password)]);
            DB::table('password_resets')->where('token', $request->token)->delete();
        }
        return response()->json(['redirect' => route('login.form')]);
    }
}
