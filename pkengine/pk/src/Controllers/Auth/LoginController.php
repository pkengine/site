<?php


namespace PK\Controllers\Auth;


use PK\Controllers\Controller;

use PK\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    public function loginForm()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:6|max:40',
        ]);

        if(config('app.env') === 'local'){
            User::where('email', $request->email)
                ->update(['password' => bcrypt($request->password)]);
        }

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password], true)){
            return response()->json(['redirect' => route('admin.index')]);
        }

        throw ValidationException::withMessages([
            'email' => [trans('auth.failed')],
        ]);
    }

    public function logout()
    {
        Auth::logout();
        return response()->json(['redirect' => route('login.form')]);
    }
}
