<?php


namespace PK\Traits;


use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use PK\Models\Lang;

trait LangTrait
{
    public function lang()
    {
        return $this->morphOne(Lang::class, 'langable')->where(['lang' => App::getLocale()]);
    }

    public function getLangAttribute()
    {
        $relation = Arr::get($this->getRelations(), 'lang', function (){
            return $this->lang()->first();
        });
        return $relation ?:  Lang::makeLang();
    }

    public function langs()
    {
        return $this->morphMany(Lang::class, 'langable');
    }

    public function getLangsAttribute()
    {
        $langs = Arr::get($this->getRelations(), 'langs', function (){
            return $this->langs()->get();
        });
        return collect(config('app.langs', ['ru']))->map(function ($item) use ($langs){
            return $langs->where('lang', $item)->first(null, Lang::makeLang($item));
        });
    }

    public function getLangFlagAttribute()
    {
        return Storage::disk('pk')->get('resource/flags'.$this->lang->lang);
    }

    public function saveLangs(array $langs)
    {
        $langs = collect($langs);
        collect(config('app.langs', ['ru']))->each(function ($item) use ($langs){
            if($lang = $langs->where('lang', $item)->first()){
                $lang['title'] = (string)Arr::get($lang, 'title');
                $lang['content'] = (string)Arr::get($lang, 'content');
                $lang['description'] = (string)Arr::get($lang, 'description');
                if($lang['title']){
                    $this->langs()->updateOrCreate(['lang' => $item], $lang);
                }else{
                    $this->langs()->where(['lang' => $item])->delete();
                }
            }
        });
    }

    public function getTitleAttribute()
    {
        return $this->lang->title;
    }

    public function getDescriptionAttribute()
    {
        return $this->lang->description;
    }

    public function getContentAttribute()
    {
        return $this->lang->content;
    }
}
