<?php


namespace PK\Traits;

use Carbon\Carbon;


trait TimezoneAtTrait
{
    public function atTz($field, $format = null)
    {
        $date = null;
        if($this->$field instanceof Carbon){
            $date = $this->$field;
        }elseif($this->$field){
            try{
                $date = Carbon::parse($this->$field);
            }catch(\Exception $e){}
        }

        if($date){
            if($format){
                if(method_exists($date, $format)){
                    return $date->$format();
                }elseif(strripos($format, '%') === false){
                    return $date->format($format);
                }else{
                    return $date->formatLocalized($format);
                }
            }
            return $date->format('Y-m-d H:i:sO');
        }
        return $format ? '' : null;
    }
}
