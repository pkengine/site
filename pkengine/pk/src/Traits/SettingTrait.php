<?php


namespace PK\Traits;


use Illuminate\Support\Arr;
use PK\Facades\Settings;
use PK\Models\Setting;

trait SettingTrait
{
    public function getMethodSettingsAttribute()
    {
        return 'get'.class_basename(static::class).'Settings';
    }

    public function getValidateSettingsAttribute()
    {
        return 'validate'.class_basename(static::class);
    }

    public function getExcludeLangAttribute()
    {
        return 'exclude'.class_basename(static::class).'Lang';
    }

    public function settings()
    {
        return $this->morphMany(Setting::class, 'settingable');
    }

    public function getSettings()
    {
        if(Arr::has($this->getRelations(), 'settings')){
            $settings = collect(Settings::provider()->{$this->method_settings}());
            $settingsModels = $this->settings->keyBy('name');
            $return = $this->prepareSettings($settings->only(Settings::provider()->{$this->exclude_lang}), $settingsModels, null);
            $langs = collect(config('app.langs', ['ru']))->mapWithKeys(function ($lang) use ($settings, $settingsModels, &$return){
                return [$lang => $this->prepareSettings($settings->except(Settings::provider()->{$this->exclude_lang}), $settingsModels, $lang)];
            });
            $return->put('langs', $langs);
            return $return;
        }else{
            return new \stdClass();
        }
    }

    public function getSetting($name)
    {
        if($settingsModel = $this->settings->where('name', $name)->first()){
            return Settings::get($settingsModel, true, false);
        }else{
            $settings = collect(Settings::provider()->{$this->method_settings}());
            return Settings::prepareType(Arr::get($settings, $name));
        }
    }

    protected function prepareSettings($settings, $settingsModels, $lang)
    {
        return $settings->map(function ($type, $name) use ($settingsModels, $lang){
            if($settingsModel = $settingsModels->filter(function ($item) use ($name, $lang){
                return $item->name === $name && $item->lang === $lang;
            })->first()){
                return Settings::get($settingsModel, true, false);
            }else{
                return Settings::prepareType($type);
            }
        });
    }

    protected static function getValidate()
    {
        $instance = new static();
        $validations = (array)Settings::provider()->{$instance->validate_settings}();
        $return = collect($validations)->only(Settings::provider()->{$instance->exclude_lang})->mapWithKeys(function ($value, $key){
            return ['settings.'.$key => $value];
        })->toArray();
        $langs = collect(config('app.langs', ['ru']))->mapWithKeys(function ($item) use ($validations, $instance){
            return collect($validations)->except(Settings::provider()->{$instance->exclude_lang})->mapWithKeys(function ($value, $key) use ($item){
                return ['settings.'.$item.'.'.$key => $value];
            });
        })->toArray();
        $return = array_merge($return, $langs);
        return $return;
    }

    public function setSetting(string $name, $value, $type = 'string', $lang = null)
    {
        if(in_array($name, Settings::provider()->{$this->exclude_lang})){
            $lang = null;
            $this->settings()->withoutGlobalScope('lang')->where(['name' => $name])->whereNotNull('lang')->delete();
        }else{
            $this->settings()->withoutGlobalScope('lang')->where(['name' => $name])->whereNull('lang')->delete();
            if(!$lang) return false;
        }
        $data = [
            'value' => json_encode($value),
            'type' => $type
        ];
        return $this->settings()->withoutGlobalScope('lang')->updateOrCreate(['name' => $name, 'lang' => $lang], $data);
    }

    public function saveSettings($settings)
    {
        foreach(Arr::get($settings, 'langs') as $lang => $items){
            collect(Settings::provider()->{$this->method_settings}())
                ->except(Settings::provider()->{$this->exclude_lang})
                ->each(function ($type, $name) use ($lang, $items){
                    $this->setSetting($name, Arr::get($items, $name), $type, $lang);
                });
        }
        unset($settings['langs']);
        collect(Settings::provider()->{$this->method_settings}())
            ->only(Settings::provider()->{$this->exclude_lang})
            ->each(function ($type, $name) use ($settings){
                $this->setSetting($name, Arr::get($settings, $name), $type, null);
            });
    }
}
