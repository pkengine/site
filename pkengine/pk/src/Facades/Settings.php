<?php


namespace PK\Facades;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Str;
use PK\Models\Page;

class Settings extends Facade
{
    protected static function getFacadeAccessor() { return 'settings'; }

    public static function menuItem($item)
    {
        switch(Arr::get($item, 'type')){
            case 'page':
            case 'category':
                try {
                    $class = '\PK\Models\\'.Str::studly(Arr::get($item, 'type'));
                    if(Arr::get($item, 'id') && ($model = $class::find(Arr::get($item, 'id')))){
                        return route(Arr::get($item, 'type'), ['slug' => (string)$model->slug]);
                    }
                }catch(\Exception $e){
                    return '#';
                }
                break;
            default:
                return Arr::get($item, 'href');
        }
    }
}
