<?php


namespace PK\Facades;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Facade;
use PK\Models\Page;

class Langs extends Facade
{
    protected static function getFacadeAccessor() { return 'langs'; }
}
