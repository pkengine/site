<?php

namespace PK\Rules;

use Illuminate\Contracts\Validation\Rule;
use PK\Models\Category;

class CategoryParentRule implements Rule
{
    protected $excludeIds = [];

    /**
     * Create a new rule instance.
     * @param Category $category
     * @return void
     */
    public function __construct(Category $category)
    {
        $ids = $category->children_recursive_ids;
        $ids[] = $category->id;
        $this->excludeIds = $ids;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return !in_array((int)$value, $this->excludeIds);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The validation error message.';
    }
}
