<?php

namespace PK\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class ImageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'path' => $this->path,
            'path_thumb' => $this->path_thumb,
            'path_thumb_p' => $this->path_thumb_p,
            'alt' => $this->alt,
            'size' => $this->size,
            'width' => $this->width,
            'height' => $this->height,
            'width_thumb' => $this->width_thumb,
            'height_thumb' => $this->height_thumb,
        ];
    }
}
