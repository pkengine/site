<?php

namespace PK\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;

class Lang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(($lang = Session::get('lang')) && !$request->is('admin/*')){
            Config::set('app.locale', $lang);
            Config::set('app.fallback_locale', $lang);
            App::setLocale($lang);
            $locale = $lang . '_' . mb_strtoupper($lang) . '.UTF-8';//'ru_RU.UTF-8'
            setlocale(LC_ALL, $locale);
        }

        return $next($request);
    }
}
