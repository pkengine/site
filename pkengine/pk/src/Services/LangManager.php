<?php


namespace PK\Services;


use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use PK\Models\Setting;
use Illuminate\Support\Arr;

class LangManager
{


    /**
     * @return array
     */
    public function get()
    {
        return collect(config('app.langs', ['ru']))->map(function ($item){
            return [
                'name' => $item,
                'is_active' => $item === App::getLocale(),
                'icon' => Storage::disk('pk')->get('/resources/flags/'.$item.'.svg')
            ];
        });
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function set(string $name)
    {

    }
}
