<?php

namespace PK\Models;

use PK\Traits\ImageTrait;
use PK\Traits\SettingTrait;
use PK\Traits\SlugTrait;
use PK\Traits\LangTrait;
use Illuminate\Database\Eloquent\Builder;

class Category extends EloquentModel
{
    use SlugTrait, LangTrait, SettingTrait, ImageTrait;

    const FULL_RELATIONS = ['settings', 'parent', 'children'];

    protected $fillable = [
        'slug', 'is_publish', 'image_id', 'parent_id', 'sort'
    ];

    protected static function booted()
    {
        static::creating(function($model){
            if($last = Category::orderByDesc('sort')->first()){
                $sort = $last->sort;
            }else{
                $sort = 1;
            }
            $model->sort = $sort;
        });
        static::addGlobalScope('lang', function (Builder $builder) {
            $builder->with('lang', 'langs');
        });

        static::addGlobalScope('image', function (Builder $builder) {
            $builder->with('image');
        });
    }

    public function post()
    {
        return $this->hasOne(Post::class)->latest();
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id')->sorted();
    }

    public function childrenRecursive()
    {
        return $this->hasMany(Category::class, 'parent_id')
            ->sorted()
            ->with('childrenRecursive');
    }

    public function getChildrenRecursiveIdsAttribute()
    {
        return $this->parseChildrenRecursiveIds($this->childrenRecursive);
    }

    protected function parseChildrenRecursiveIds($items)
    {
        $ids = [];
        foreach ($items as $item){
            $ids[] = $item->id;
            $childrenIds = $this->parseChildrenRecursiveIds($item->children);
            $ids = array_merge($ids, $childrenIds);
        }
        return $ids;
    }

    public function parent()
    {
        return $this->belongsTo(Category::class);
    }

    public function parentRecursive()
    {
        return $this->belongsTo(Category::class, 'parent_id')->with('parentRecursive');
    }

    public function posts()
    {

        return $this->hasMany(Post::class);
    }

    public function items()
    {
        return $this->belongsToMany(Post::class, 'category_post');
    }

    public static function scopeCategory($query, $slug)
    {
        $query->where('category', $slug);
    }

    public function scopeWithSlug($query)
    {
        $query->whereNotNull('slug')->where('slug', '!=', '');
    }

    public function getStepAttribute()
    {
        return $this->getOriginal('pivot_step');
    }

    public function scopeSorted($query)
    {
        $query->orderBy('sort');
    }
}
