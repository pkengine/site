<?php

namespace PK\Models;

use PK\Traits\ImageTrait;
use PK\Traits\SettingTrait;
use PK\Traits\SlugTrait;
use PK\Traits\LangTrait;
use Illuminate\Database\Eloquent\Builder;

class Taxonomy extends EloquentModel
{
    use SlugTrait, LangTrait;

    const FULL_RELATIONS = [];

    protected $fillable = [
        'slug', 'is_publish'
    ];

    protected static function booted()
    {
        static::addGlobalScope('lang', function (Builder $builder) {
            $builder->with('lang', 'langs');
        });

        static::addGlobalScope('image', function (Builder $builder) {
            $builder->with('image');
        });
    }
}
