<?php

namespace PK\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\App;
use PK\Traits\SlugTrait;
use PK\Traits\TimezoneAtTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image as ImageIntervention;

class Lang extends EloquentModel
{

    protected $fillable = [
        'title', 'description', 'content', 'lang'
    ];

    public function langable()
    {
        return $this->morphTo();
    }

    protected static function booted()
    {

    }

    /**
     * @param null|string $lang
     * @return Lang
     */
    public static function makeLang($lang = null)
    {

        $lang = $lang ?: App::getLocale();
        $model = static::make();
        $model->fill(['lang' => $lang]);
        return $model;
    }
}
