<?php

namespace PK\Models;

use PK\Traits\ImageTrait;
use PK\Traits\SettingTrait;
use PK\Traits\SlugTrait;
use PK\Traits\LangTrait;
use Illuminate\Database\Eloquent\Builder;

class Tag extends EloquentModel
{
    use SlugTrait, LangTrait, SettingTrait, ImageTrait;

    const FULL_RELATIONS = ['settings'];

    protected $fillable = [
        'slug', 'is_publish', 'image_id', 'taxonomy'
    ];

    protected static function booted()
    {
        static::addGlobalScope('lang', function (Builder $builder) {
            $builder->with('lang', 'langs');
        });

        static::addGlobalScope('image', function (Builder $builder) {
            $builder->with('image');
        });
    }

    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }

    public function scopeWithSlug($query)
    {
        $query->whereNotNull('slug')->where('slug', '!=', '');
    }
}
