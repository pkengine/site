<?php
namespace PK\Models;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use PK\Traits\ImageTrait;
use PK\Traits\SettingTrait;
use PK\Traits\SlugTrait;
use PK\Traits\LangTrait;

class Page extends EloquentModel
{
    use SlugTrait, LangTrait, SettingTrait, ImageTrait;

    const FULL_RELATIONS = ['settings'];

    protected $fillable = [
        'slug', 'template', 'image_id'
    ];

    protected static function booted()
    {
        static::addGlobalScope('lang', function (Builder $builder) {
            $builder->with('lang');
            $builder->with('langs');
        });

        static::addGlobalScope('image', function (Builder $builder) {
            $builder->with('image');
        });
    }
}
