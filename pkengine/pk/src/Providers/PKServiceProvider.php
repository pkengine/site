<?php

namespace PK\Providers;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use PK\Facades\Settings;
use PK\Middleware\Install;
use PK\Services\LangManager;
use PK\Services\SettingManager;
use Illuminate\Support\ServiceProvider;

class PKServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('settings',function() {
            return new SettingManager();
        });
        $this->app->bind('langs',function() {
            return new LangManager();
        });
        $this->app['config']['filesystems.disks.pk'] = [
            'driver' => 'local',
            'root' => __DIR__.'/../../',
        ];
        $this->app['translator']->addNamespace('pk', Storage::disk('pk')->path('resources/lang'));

        AliasLoader::getInstance(['Front' => \PK\Helpers\FrontHelper::class]);
        AliasLoader::getInstance(['Image' => \Intervention\Image\Facades\Image::class]);
        AliasLoader::getInstance(['Settings' => Settings::class]);
        $this->loadRoutesFrom(__DIR__.'/../../routes/admin.php');
        $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
        $this->loadViewsFrom(__DIR__.'/../../resources/views/admin', 'admin');
        $this->loadViewsFrom(__DIR__.'/../../resources/views/install', 'install');
        $this->publishes([
            __DIR__.'/../../config/' => config_path(),
        ], 'config');
        $this->publishes([
            __DIR__.'/../../resources/lang/' => resource_path('lang'),
        ], 'lang');



        Blade::directive('hasSetting', function ($expression) {
            return "<?php if('\PK\Facades\Settings::get('{$expression}')) { ?>";
        });
        Blade::directive('setting', function ($expression) {
            return "<?php echo \PK\Facades\Settings::get('{$expression}'); ?>";
        });

    }
}
