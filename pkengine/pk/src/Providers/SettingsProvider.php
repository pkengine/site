<?php


namespace PK\Providers;


use Illuminate\Support\Str;

class SettingsProvider
{
    public $excludeLang = [];

    public $relations = [];

    public function validate()
    {
        return [];
    }

    public function getSettings()
    {
        return [];
    }


    public function __call($method, $args)
    {
        if(Str::startsWith($method, 'get') && Str::endsWith($method, 'Settings')){
            if(method_exists($this, $method)){
                return $this->$method();
            }
            return [];
        }
    }

    public function __get($var)
    {
        if(Str::startsWith($var, 'exclude') && Str::endsWith($var, 'Lang')){
            if(property_exists($this, $var)){
                return $this->$var;
            }
            return [];
        }
    }
}
