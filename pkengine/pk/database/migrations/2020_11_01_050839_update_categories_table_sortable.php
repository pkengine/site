<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCategoriesTableSortable extends Migration
{
    protected $i = 1;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->integer('sort')->nullable();
        });

        \Illuminate\Support\Facades\DB::table('categories')
            ->orderBy('id')
            ->chunk(100, function ($items){
                foreach ($items as $item){
                    \Illuminate\Support\Facades\DB::table('categories')
                        ->where('id', $item->id)
                        ->update(['sort' => $this->i++]);
                }
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('sort');
        });
    }
}
