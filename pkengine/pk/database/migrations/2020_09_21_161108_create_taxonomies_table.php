<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaxonomiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('taxonomies', function (Blueprint $table) {
            $table->id();
            $table->string('slug')->nullable();
            $table->timestamps();
        });

        Schema::table('categories', function (Blueprint $table) {
            $table->unsignedBigInteger('taxonomy_id')->nullable();
            $table->foreign('taxonomy_id')->references('id')->on('taxonomies')->onDelete('set null');
        });

        Schema::table('tags', function (Blueprint $table) {
            $table->unsignedBigInteger('taxonomy_id')->nullable();
            $table->foreign('taxonomy_id')->references('id')->on('taxonomies')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropForeign(['taxonomy_id'])->nullable();
            $table->dropColumn(['taxonomy_id'])->nullable();
        });
        Schema::table('tags', function (Blueprint $table) {
            $table->dropForeign(['taxonomy_id'])->nullable();
            $table->dropColumn(['taxonomy_id'])->nullable();
        });
        Schema::dropIfExists('taxonomies');
    }
}
