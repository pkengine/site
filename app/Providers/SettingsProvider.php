<?php


namespace App\Providers;


use PK\Models\Image;
use PK\Models\Post;

class SettingsProvider extends \PK\Providers\SettingsProvider
{
    protected $settings = [

    ];

    public $excludeLang = [
    ];

    public $excludePostLang = [
    ];

    protected $front = [
        'main_title', 'main_description'
    ];

    public $relations = [
    ];

    public function validate()
    {
        return array_merge(
            [
                'description' => 'nullable|string|max:1000',
                'address' => 'nullable|string|max:1000',
                'email' => 'nullable|email',
                'scripts' => 'nullable|string',
                'phone' => 'nullable|string|max:1000',
                'vk' => 'nullable|string|max:1000',
                'facebook' => 'nullable|string|max:1000',
                'instagram' => 'nullable|string|max:1000',
                'main_menu' => 'nullable|array',
            ],
            collect($this->front)->mapWithKeys(function ($item){
                return ['front_'.$item => 'nullable|string|max:1000'];
            })->toArray()
        );
    }

    public function getSettings()
    {
        return array_merge(
            $this->settings,
            collect($this->front)->mapWithKeys(function ($item){
                return ['front_'.$item => 'string'];
            })->toArray()
        );
    }

    public function getPostSettings()
    {
        return [];
    }

    public function validatePost()
    {
        return [];
    }
}
