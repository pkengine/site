<?php

namespace App\Http\Controllers;

use App\Mail\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use PK\Facades\Settings;

class FormController extends Controller
{
    public function send(Request $request, string $type)
    {
        if(method_exists($this, $type.'Form')){
            $requestData = $this->{$request->type.'Form'}($request);
        }else{
            $requestData = $this->defaultForm($request);
        }
        $requestData['type'] = $type;
        if($email = Settings::get('email')){
            try{
                Mail::to($email)->send(new FormRequest($requestData));
            }catch (\Exception $e){

            }
        }
        return response()->json(['message' => __('front.request.success')]);
    }

    protected function requestValidate($validate = [])
    {
        $validate = array_merge($validate, [
            'name' => 'required|string|min:1|max:40',
            'phone' => 'required|string|min:7|max:20',
            'email' => 'required|email',
            'page' => 'required|string|min:5|max:200',
        ]);
        return app('request')->validate($validate);
    }

    protected function defaultForm()
    {
        return $this->requestValidate();
    }
}
