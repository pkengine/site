<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends AppController
{
    public function index()
    {
        return view('app.index');
    }

    public function data()
    {
        return response()->json([]);
    }

    public function uikit()
    {
        return view('uikit');
    }
}
