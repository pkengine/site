<?php

namespace App\Http\Controllers;

use Illuminate\Support\Arr;
use PK\Models\Post;
use Illuminate\Http\Request;

class PostController extends AppController
{

    public function show($slug)
    {
        if(!is_string($slug) || !($post = Post::query()->whereSlug($slug)->adminWithTrashed()->first())) abort(404);
        return view('app.post', [
            'model' => $post,
            'slug' => $slug
        ]);
    }
}
