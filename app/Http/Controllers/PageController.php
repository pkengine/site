<?php

namespace App\Http\Controllers;

use PK\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class PageController extends AppController
{
    public function show($slug)
    {
        if(!is_string($slug) || !($page = Page::whereSlug($slug)->first())) abort(404);
        return view('app.page', [
            'model' => $page,
            'slug' => $slug
        ]);
    }
}
