<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        DB::statement('ALTER TABLE users AUTO_INCREMENT = 1');
        \PK\Models\User::create([
            'first_name' => 'Admin',
            'last_name' => '',
            'email' => 'admin@email.test',
            'password' => bcrypt('secret'),
            'role' => 'ADMIN',
            'user_type' => 1,
        ]);
    }
}
