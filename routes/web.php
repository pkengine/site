<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')->name('index');
Route::get('data', 'IndexController@data')->name('data');
Route::get('/post/{slug}', 'PostController@show')->name('post');
Route::get('/page/{slug}', 'PageController@show')->name('page');
Route::get('/category/{slug}', 'PostController@category')->name('category');
Route::post('/form/{type}', 'FormController@send')->name('form');

Route::get('uikit', 'IndexController@uikit')->name('uikit');
