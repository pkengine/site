<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@php
    $title = isset($title) ? $title : (isset($model) ? $model->title : null);
    $description = isset($model) ? $model->description : null;
@endphp
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Ribs&beers {{ $title ?? '' }}</title>
    <meta name="description" content="{{ $description }}">
    @stack('head')
</head>
<body>
@yield('main-content')
</body>
</html>
