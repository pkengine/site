<div class="uk-section uk-section-secondary uk-light">
    <div class="uk-padding"></div>
    <div class="uk-container">
        <h1 class="uk-text-center">{{ $model->title }}</h1>
        <div class="uk-width-xlarge uk-margin-auto uk-margin uk-text-muted uk-text-center">{{ $model->description }}</div>
        <div>{!! $model->content !!}</div>
    </div>
</div>
