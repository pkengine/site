@extends('app.layout')
@section('content')
    @includeFirst(['app.pages.'.$slug, 'app.pages.default'], ['model' => $model])
@endsection
