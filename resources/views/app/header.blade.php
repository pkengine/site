<div
    class="main-menu uk-section uk-section-xsmall uk-width-1-1 uk-light"
>
    <div class="uk-container">
        <div class="uk-grid" uk-grid>
            <div class="uk-width-auto uk-visible@m">
                <div class="uk-child-width-auto uk-height-1-1 uk-flex-center" uk-grid>
                    @foreach((array)\PK\Facades\Settings::get('main_menu') as $item)
                        <div class="uk-flex uk-flex-middle">
                            <a class="uk-link-reset" href="{{ \PK\Facades\Settings::menuItem($item) }}">{{ \Illuminate\Support\Arr::get($item, 'name') }}</a>
                        </div>
                    @endforeach
                    @foreach(\PK\Facades\Langs::get() as $lang)
                        @if(!$lang['is_active'])
                            <div class="uk-flex uk-flex-middle">
                                <a class="uk-link-reset flag" @click="setLang('{{ $lang['name'] }}')">
                                    {!! $lang['icon'] !!}
                                </a>
                            </div>
                        @endif
                    @endforeach
                    <div class="uk-flex uk-flex-middle">
                        <a class="uk-link-reset fa fa-search"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="menu-offcanvas" uk-offcanvas>
    <div class="uk-offcanvas-bar">
        <button class="uk-offcanvas-close" type="button" uk-close></button>
        <div>
            <ul class="uk-nav uk-nav-default">
                @foreach((array)\PK\Facades\Settings::get('main_menu') as $item)
                    <li>
                        <a href="{{ \PK\Facades\Settings::menuItem($item) }}">{{ \Illuminate\Support\Arr::get($item, 'name') }}</a>
                    </li>
                @endforeach
            </ul>
        </div>

        <hr>
        <div class="uk-margin-small">
            @if($string = \PK\Facades\Settings::get('phone'))
                <a href="tel:{{ $string }}" class="uk-link-heading">
                    <i class="far fa-phone"></i>
                    <span>{{ $string }}</span>
                </a>
            @endif
        </div>
        <div class="uk-margin-small">
            @if($string = \PK\Facades\Settings::get('email'))
                <a href="mailto:{{ $string }}" class="uk-link-heading">
                    <i class="far fa-phone"></i>
                    <span>{{ $string }}</span>
                </a>
            @endif
        </div>
        <div class="uk-margin-small">
            @if($string = \PK\Facades\Settings::get('vk'))
                <a href="{{ $string }}" target="_blank" class="uk-link-heading">
                    <i class="fab fa-vk"></i>
                </a>
            @endif
            @if($string = \PK\Facades\Settings::get('facebook'))
                <a href="{{ $string }}" target="_blank" class="uk-link-heading">
                    <i class="fab fa-facebook"></i>
                </a>
            @endif
            @if($string = \PK\Facades\Settings::get('instagram'))
                <a href="{{ $string }}" target="_blank" class="uk-link-heading">
                    <i class="fab fa-instagram"></i>
                </a>
            @endif
        </div>
    </div>
</div>
