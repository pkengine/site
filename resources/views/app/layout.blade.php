@extends('layout')
@push('head')
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link rel="stylesheet" type="text/css" href="{{ Front::assetV('/css/app.css')}}">
    <link rel="icon" type="image/png" href="/storage/images/favicon.png" />
@endpush
@section('main-content')
    <div id="app">
        @include('app.header')
        @yield('content')
        @include('app.footer')
    </div>
    <script src="{{ Front::assetV('/js/app.js')}}"></script>
    <script>
        {{ \PK\Facades\Settings::get('scripts') }}
    </script>
@endsection
