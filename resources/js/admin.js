import Vue from 'vue';
import PostEdit from "./components/PageEdit"
import SettingsPage from "./components/SettingsPage"
import PostProductsSidebarEdit from "./components/PostProductsSidebarEdit";

require('./../../pkengine/pk/resources/js/admin/admin');

$app.$customComponents = [
    'post-products-sidebar-edit', 'page-edit'
]

Vue.component('post-products-sidebar-edit', PostProductsSidebarEdit);
Vue.component('page-edit', PostEdit);
Vue.component('settings-page', SettingsPage);
