/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.token = document.head.querySelector('meta[name="csrf-token"]');
window.UIkit = require('uikit');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}
import _ from 'lodash';
import Vue from 'vue';
Vue.csrf = Vue.prototype.$csrf = token.content

// import Echo from "laravel-echo"
// window.io = require('socket.io-client');
// window.Echo = new Echo({
//     broadcaster: 'socket.io',
//     host: window.location.hostname + ':6003',
//     auth: {
//         headers: {
//             'X-CSRF-TOKEN': token.content
//         }
//     }
// });

import VueTheMask from 'vue-the-mask'
import Auth from './../../pkengine/pk/resources/js/auth/Auth';
import DatePicker from 'vue2-datepicker';
import 'vue2-datepicker/locale/ru';
import 'vue2-datepicker/locale/en';
import 'vue2-datepicker/locale/cs';

Vue.use(VueTheMask)
Vue.component('Auth', Auth)
const app  = new Vue({
    el: '#app',
    components: { DatePicker },
    data(){
        return {
            form: {
                type: '',
                title: '',
                button: '',
                confirm: false,
                data: {
                    name: '',
                    phone: '',
                    person_count: '',
                    date: null,
                    email: '',
                    page: window.location.href
                }
            },
            isLoad: false,
            isScroll: false,
            errors: {
                form: {},
                auth: {}
            },
            price: 0,
            responseStatusCode: null,
            formActive: false
        }
    },
    methods: {
        logout(){
            axios.post('/logout')
                .then((response)=> {
                    window.location.href = response.data.redirect
                })
        },
        showModal(id){
            let el = document.getElementById(id)
            UIkit.modal(el).show();
        },
        showForm(type, title, button){
            this.form.type = type
            this.form.button = button ? button : 'Отправить'
            this.form.title = title ? title : 'Отправить заявку'
            this.showModal('request-form')
        },
        sendForm(type, data){
            type = type ? type : this.form.type
            data = data ? data : this.form.data
            if(type && _.isString(type) && type.length){
                this.errors.form = {}
                axios.post('/form/' + type, data)
                    .then((response) => {
                        UIkit.notification({
                            message: response.data.message,
                            status: 'success',
                            pos: 'top-center',
                            timeout: 5000
                        });
                        Object.keys(this.form.data).forEach((index) => {
                            if(_.isString(this.form.data[index])){
                                this.form.data[index] = ''
                            }else if(_.isBoolean(this.form.data[index])){
                                this.form.data[index] = false
                            }
                        });
                    })
                    .catch((error) => {
                        this.validatorErrors(error, 'form');
                    })
            }

        },
        isScrollEvent(){
            this.isScroll = window.scrollY > 50
        },

        validatorErrors(error, prefix) {
            if (_.has(error, 'response.data.errors')) {
                let errors = _.mapValues(_.get(error, 'response.data.errors', {}), 0);
                if(prefix){
                    this.errors[prefix] = errors
                }else{
                    this.errors = errors
                }
            }
        },
        setLang(lang){
            axios.post('/lang/' + lang)
                .then(() => {
                    window.location.reload()
                })
        }
    },
    created(){
        window.addEventListener('scroll', () => {
            this.isScrollEvent()
        });

        this.isScrollEvent()
        axios.get('/data')
            .then((response) => {
                this.isLoad = true
            })
    }
});
